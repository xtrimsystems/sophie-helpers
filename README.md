# Sophie Helpers & Mixins

Provides some helper function and mixins

- Browser related functions: getHTMLElement, getHTMLElements
- Mixins
  - Publish/Subscription pattern
  - [Custom Event Target](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget)

**Part of the framework [Sophie](https://bitbucket.org/xtrimsystems/sophie)**

[![npm version](https://badge.fury.io/js/sophie-helpers.svg)](https://badge.fury.io/js/sophie-helpers)
[![npm dependencies](https://david-dm.org/xtrimsystems/sophie-helpers.svg)](https://www.npmjs.com/package/sophie-helpers?activeTab=dependencies)
[![npm downloads](https://img.shields.io/npm/dm/sophie-helpers.svg)](https://www.npmjs.com/package/sophie-helpers)

## INSTALLATION
```shell
yarn add sophie-helpers
```

## USAGE

## Changelog
[Changelog](https://bitbucket.org/xtrimsystems/sophie-helpers/src/master/CHANGELOG.md)

## Contributing [![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

## License
This software is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT). See [LICENSE](https://bitbucket.org/xtrimsystems/sophie-helpers/src/master/LICENSE) for the full license.
