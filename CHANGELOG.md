# Changelog

All notable changes are documented in this file using the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [1.2.0-beta] - 2019-03-30

### Added

* Collections
* TsLint rules closer to recommended

## [1.0.2-beta] - 2018-07-15

### Added

* First version finalized.
