/*tslint:disable no-any*/
import { PublisherSubscriber } from './PublisherSubscriber';

export interface Subscription
{
	event: string;
	subscriber: PublisherSubscriber;
	callback (...args: any[]): any;
}
