import { Subscription } from './Subscription';
import { Uuid } from './Uuid';

/*tslint:disable no-any*/

export class PublisherSubscriber
{
	protected uuid: Uuid = new Uuid();
	protected subscriptions: Subscription[] = [];

	public subscribe (subscribeRequest: Subscription): void
	{
		this.subscriptions.push(subscribeRequest);
	}

	public publish (eventName: string, ...args: any[]): void
	{
		const subscriptions: Subscription[] = this.subscriptions.filter(
			(subscription: Subscription) => subscription.event === eventName,
		);

		for (const subscription of subscriptions) {
			subscription.callback(...args);
		}
	}

	public unSubscribe (subscriber: PublisherSubscriber, eventName: string)
	{
		this.subscriptions = this.subscriptions.filter(
			(subscription) =>
				subscription.subscriber.uuid.asString() !== subscriber.uuid.asString() ||
					subscription.event !== eventName);
	}

	public unSubscribeAll (subscriber: PublisherSubscriber)
	{
		this.subscriptions = this.subscriptions.filter(
			(subscription) => subscription.subscriber.uuid.asString() !== subscriber.uuid.asString(),
		);
	}
}
