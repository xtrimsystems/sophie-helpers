/*tslint:disable no-magic-numbers*/

const SEPARATOR = '-';
const SEPARATOR_POSITION_ARRAY = [8, 12, 16, 20];

function generateUuid (): string // 96657965-d638-7cba-a257-b76ae4a86650
{
	let uuid = '';

	for (let i = 0; i < 32; i++) {
		const random = Math.random() * 16 | 0;

		if (~SEPARATOR_POSITION_ARRAY.indexOf(i)) {
			uuid += SEPARATOR;
		}

		uuid += (i === 12 ? 7 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
	}

	return uuid;
}
/*tslint:enable no-magic-numbers*/

export class Uuid
{
	private readonly value: string;

	public constructor ()
	{
		this.value = generateUuid();
	}

	public asString (): string
	{
		return this.value;
	}
}
