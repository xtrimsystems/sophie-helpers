/* tslint:disable no-any */

export type Constructor<T> = new (...args: any[]) => T;
export type Mixin<T> = Constructor<T> | object;

function mix (client: Constructor<any>, mixins: Array<Mixin<any>>)
{
	const clientKeys = Object.getOwnPropertyNames(client.prototype);

	for (const mixin of mixins) {
		const mixinMixables = getMixables(clientKeys, mixin);
		Object.defineProperties(client.prototype, mixinMixables);
	}
}

/**
 * Returns a map of mixables. That is things that can be mixed in
 */
function getMixables (clientKeys: string[], mixin: Mixin<any>)
{
	let descriptors: PropertyDescriptorMap = {};

	switch (typeof mixin) {
		case 'object':
			descriptors = _getMixables(clientKeys, mixin);
			break;
		case 'function':
			descriptors = _getMixables(clientKeys, (mixin as Constructor<any>).prototype);
			break;
		default:
			throw new Error('Mixin type not valid, is not an object nor a function');
	}

	return descriptors;

}

function _getMixables (clientKeys: string[], obj: object): PropertyDescriptorMap {

	const map: PropertyDescriptorMap = {};

	Object.getOwnPropertyNames(obj)
		.map((key) => {
			if (clientKeys.indexOf(key) < 0) {

				const descriptor = Object.getOwnPropertyDescriptor(obj, key);

				if (descriptor === undefined) {
					return;
				}

				/* tslint:disable no-unbound-method */
				if (descriptor.get || descriptor.set) {
					/* tslint:enable no-unbound-method */
					map[ key ] = descriptor;
				} else if (typeof descriptor.value === 'function') {
					map[ key ] = descriptor;
				}
			}
		});

	return map;
}

/**
 * Takes a list of classes or object literals and adds their methods
 * to the class calling it.
 */
export function use (...args: Array<Mixin<any>>)
{
	return (target: any, propertyKey: string) => {
		mix(target.constructor, args.reverse());
	};
}
