export function bubbleUpToTag (el: HTMLElement, tagName: string): HTMLElement | false
{
	if ((el.nodeName || el.tagName).toLowerCase() === tagName.toLowerCase()) {
		return el;
	}

	do {
		// tslint:disable no-parameter-reassignment
		el = el.parentNode as HTMLElement;
		if (el && (el.nodeName || el.tagName).toLowerCase() === tagName.toLowerCase()) {
			return el;
		}
	}
	while (el);

	return false;
}

export function getHTMLElement (element: Element, query: string): HTMLElement
{
	const elementSelected: Element | null = element.querySelector(query);

	if (!elementSelected)
	{
		throw new Error(`Not found element with selector ${query}`);
	}

	return elementSelected as HTMLElement;
}

export function getHTMLElements (element: Element, query: string): NodeList
{
	const elementsSelected: NodeList | null = element.querySelectorAll(query);

	if (!elementsSelected || elementsSelected.length === 0)
	{
		throw new Error(`No found elements with selector ${query}`);
	}

	return elementsSelected;
}
