export class Collection<T>
{
	private items: T[] = [];

	public [Symbol.iterator] (): IterableIterator<T>
	{
		return this.iterator();
	}

	public* iterator (): IterableIterator<T>
	{
		for (const item of this.items) {
			yield item;
		}
	}

	public get length (): number
	{
		return this.items.length;
	}

	public getCollectionAsArray (): T[]
	{
		return this.items;
	}

	public has (item: T): boolean
	{
		return this.items.includes(item);
	}

	public add (items: T | T[], head?: boolean): void
	{
		if (Array.isArray(items)) {
			items.forEach((item: T) => {
				this.rawAdd(item, head);
			});
		} else {
			this.rawAdd(items, head);
		}
	}

	public remove (item: T): void
	{
		const index: number = this.items.indexOf(item);
		if (!~index) { /* tslint:disable-line no-bitwise */
			throw new Error(`Item not found in collection ${this.constructor.name}`);
		}

		this.items.splice(index, 1);
	}

	private rawAdd (items: T, head?: boolean)
	{
		if (head) {
			this.items = [items].concat(this.items);
		} else {
			this.items.push(items);
		}
	}
}
