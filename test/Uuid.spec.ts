/* tslint:disable newline-per-chained-call */

import { Uuid } from '../src/Mixins/EventHandlers/PublisherSubscriber';

describe('Test Uuid', () => {

	describe('method "asString"', () => {

		it('should return the expected string format', () => {

			const uuid = new Uuid();

			expect(uuid.asString()).toMatch(/\w{8}-(?:\w{4}-){3}\w{12}/);

		});
	});
});
