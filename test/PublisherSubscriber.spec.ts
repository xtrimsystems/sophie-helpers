/* tslint:disable newline-per-chained-call */
/* tslint:disable no-magic-numbers */
/* tslint:disable no-empty */

import { PublisherSubscriber, Subscription } from '../src';

describe('Test PublisherSubscriber', () => {

	const callback: jest.Mock = jest.fn();
	let testPubSub: TestClassPubSub;
	let test2PubSub: TestClassPubSub;

	beforeEach(() => {

		testPubSub = new TestClassPubSub();
		test2PubSub = new TestClassPubSub();

		testPubSub.subscribe({
			callback,
			event: 'eventName',
			subscriber: test2PubSub,
		});

	});

	afterEach(() => {
		jest.clearAllMocks();
	});

	describe('method "subscribe"', () => {
		it('should add a subscription', () => {

			testPubSub.subscribe({
				callback,
				event: 'eventName2',
				subscriber: test2PubSub,
			});

			expect(testPubSub.getSubscriptions().length).toBe(2);
		});
	});

	describe('method "publish"', () => {

		it('should call the callback that was subscribed for the event published', () => {

			testPubSub.publish('eventName', {foo: 'bar'});

			expect(callback).toHaveBeenCalledTimes(1);
			expect(callback).toHaveBeenCalledWith({foo: 'bar'});

		});

		it('should not call the callback that was subscribed if the event is not the same', () => {

			testPubSub.publish('eventName2');

			expect(callback).toHaveBeenCalledTimes(0);

		});
	});

	describe('unSubscriptions', () => {

		beforeEach(() => {
			testPubSub.subscribe({
				callback: () => {},
				event: 'event2Name',
				subscriber: test2PubSub,
			});
		});

		describe('method "unSubscribe', () => {
			it('should remove a subscription', () => {

				expect(testPubSub.getSubscriptions().length).toBe(2);

				testPubSub.unSubscribe(test2PubSub, 'event2Name');

				expect(testPubSub.getSubscriptions().length).toBe(1);
			});
		});

		describe('method "unSubscribeAll', () => {
			it('should remove all subscriptions', () => {

				expect(testPubSub.getSubscriptions().length).toBe(2);

				testPubSub.unSubscribeAll(test2PubSub);

				expect(testPubSub.getSubscriptions().length).toBe(0);
			});
		});
	});
});

class TestClassPubSub extends PublisherSubscriber
{
	public getSubscriptions (): Subscription[]
	{
		return this.subscriptions;
	}
}
