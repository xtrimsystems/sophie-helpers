/* tslint:disable newline-per-chained-call */
/* tslint:disable no-any */

import { CustomEventTarget } from '../src/Mixins/EventHandlers/CustomEventTarget';

describe('Test CustomEventTarget', () => {

	let customEventTarget: CustomEventTarget;
	let mockCallback: () => void;
	const event = 'click';
	const event2 = 'mouseover';

	beforeEach(() => {
		customEventTarget = new CustomEventTarget();
		mockCallback = jest.fn();
	});

	describe('method "addEventListener"', () => {

		it('should add to the stack of the event 1 callback', () => {

			customEventTarget.addEventListener(event, mockCallback);

			expect((customEventTarget as any).listeners[event].length).toBe(1);

		});

		it('should add to the stack of one event 2 callbacks and to another event 1 callback', () => {

			const mockCallback2 = jest.fn();
			const mockCallback3 = jest.fn();

			customEventTarget.addEventListener(event, mockCallback);
			customEventTarget.addEventListener(event, mockCallback2);
			customEventTarget.addEventListener(event2, mockCallback3);

			expect((customEventTarget as any).listeners[event].length).toBe(2);
			expect((customEventTarget as any).listeners[event2].length).toBe(1);

		});
	});

	describe('method "removeEventListener"', () => {

		it('should do nothing if no listeners where attach', () => {

			customEventTarget.removeEventListener(event, mockCallback);

			expect((customEventTarget as any).listeners).toBeUndefined();

		});

		it('should do nothing if the event was never listened', () => {

			customEventTarget.addEventListener(event, mockCallback);
			expect((customEventTarget as any).listeners[event].length).toBe(1);

			customEventTarget.removeEventListener(event2, mockCallback);
			expect((customEventTarget as any).listeners[event].length).toBe(1);

		});

		it('should remove the event from the listeners list', () => {

			customEventTarget.addEventListener(event, mockCallback);
			expect((customEventTarget as any).listeners[event].length).toBe(1);

			customEventTarget.removeEventListener(event, mockCallback);
			expect((customEventTarget as any).listeners[event].length).toBe(0);

		});

		it('should not remove the event from the listeners list if the callback is different', () => {

			const mockCallback2 = jest.fn();

			customEventTarget.addEventListener(event, mockCallback);
			expect((customEventTarget as any).listeners[event].length).toBe(1);

			customEventTarget.removeEventListener(event, mockCallback2);
			expect((customEventTarget as any).listeners[event].length).toBe(1);

		});
	});

	describe('method "dispatchEvent"', () => {

		it('should return true if no events are attached', () => {
			expect(customEventTarget.dispatchEvent(new CustomEvent(event))).toBe(true);
		});

		it('should call the callbacks of the events attached and return true', () => {

			customEventTarget.addEventListener(event, mockCallback);
			expect((customEventTarget as any).listeners[event].length).toBe(1);

			expect(customEventTarget.dispatchEvent(new CustomEvent(event))).toBe(true);

			expect(mockCallback).toHaveBeenCalledTimes(1);

		});
	});
});
