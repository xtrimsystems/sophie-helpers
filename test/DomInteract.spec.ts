/* tslint:disable newline-per-chained-call */
/* tslint:disable no-magic-numbers */

import { bubbleUpToTag, getHTMLElement, getHTMLElements } from '../src';

describe('Test DomInteract methods', () => {

	describe('method "bubbleUpToTag"', () => {

		it('should return the HTMLElement passed if matches the tag given', () => {
			const div = document.createElement('DIV');
			const div2 = document.createElement('SPAN');

			div.appendChild(div2);

			expect(bubbleUpToTag(div2, 'SPAN')).toEqual(div2);
		});

		it('should return the first element that matches the tag given going to recursively through parents', () => {
			const div = document.createElement('DIV');
			const div2 = document.createElement('SPAN');

			div.appendChild(div2);

			expect(bubbleUpToTag(div2, 'DIV')).toEqual(div);
		});

		it('should return false if neither the HTMLElement passed not the parents matches the tag given', () => {
			const div = document.createElement('DIV');
			const div2 = document.createElement('SPAN');

			div.appendChild(div2);

			expect(bubbleUpToTag(div2, 'LI')).toBe(false);

		});
	});

	describe('when called method "getHTMLElement"', () => {

		it('should return the expected HTMLElement if exists inside the element passed', () => {
			const div = document.createElement('DIV');
			const div2 = document.createElement('DIV');
			div2.classList.add('test');

			div.appendChild(div2);

			expect(getHTMLElement(div, '.test')).toEqual(div2);
		});

		it('should throw and exception if the element does not exists inside the element passed', () => {
			const div = document.createElement('DIV');
			expect(() => getHTMLElement(div, '.foo')).toThrow();

		});
	});

	describe('when called method "getHTMLElements"', () => {

		it('should return the expected HTMLElements if they exist inside the element passed', () => {
			const div = document.createElement('DIV');
			const div2 = document.createElement('DIV');
			const div3 = document.createElement('DIV');
			div2.classList.add('test');
			div3.classList.add('test');

			div.appendChild(div2);
			div.appendChild(div3);

			expect(getHTMLElements(div, '.test').length).toBe(2);
		});

		it('should throw and exception if the element does not exists inside the element passed', () => {
			const div = document.createElement('DIV');
			expect(() => getHTMLElements(div, '.foo')).toThrow();
		});
	});
});
