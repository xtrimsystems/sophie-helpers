/* tslint:disable max-classes-per-file */
/* tslint:disable no-conditional-assignment */
/* tslint:disable newline-per-chained-call */
/* tslint:disable no-empty */

import { use } from '../src/Mixins';

describe('Test Mixins', () => {

	describe('method "use"', () => {

		it('should enhance the class using it', () => {
			let testClass = new TestClass();

			let props: string[] = [];
			do {
				props = props.concat(Object.getOwnPropertyNames(testClass));
			} while (testClass = Object.getPrototypeOf(testClass));

			expect(!!~props.indexOf('classMethod')).toBe(true);
			expect(!!~props.indexOf('mixinMethod')).toBe(true);

		});
	});
});

const foo = { a: 1 };

class TestMixinClass
{
	public mixinMethod (): void {}
}

interface TestClass extends TestMixinClass
{
	classMethod (): void;
}

class TestClass implements TestClass
{
	@use(TestMixinClass, foo)

	public classMethod (): void {}
}
