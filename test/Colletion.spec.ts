/* tslint:disable newline-per-chained-call */
/* tslint:disable no-any */
/* tslint:disable no-magic-numbers */

import { Collection } from '../src';

describe('Test Collection', () => {

	let testCollection: Collection<string>;

	beforeEach(() => {
		testCollection = new Collection<string>();
	});

	describe('method "[Symbol.iterator]"', () => {
		it('should return the next result of the iterator', () => {
			expect(testCollection[Symbol.iterator]()).toEqual(testCollection.iterator());
		});
	});

	describe('method "iterator"', () => {
		it('should return the next result of the iterator', () => {
			(testCollection as any).items = ['a'];
			expect(testCollection.iterator()).toEqual(testCollection.iterator());
		});
	});

	describe('method "length"', () => {
		it('should return the expected length of the collection', () => {
			expect(testCollection.length).toBe(0);
		});
	});

	describe('method "getCollectionAsArray"', () => {
		it('should return the expected collection of items', () => {
			(testCollection as any).items = ['a'];
			expect(testCollection.getCollectionAsArray()).toEqual(['a']);
		});
	});

	describe('method "has"', () => {

		it('should return false if the element to search does not exist', () => {
			expect(testCollection.has('a')).toBe(false);
		});

		it('should return true if the element to search does not exist', () => {
			(testCollection as any).items = ['a'];
			expect(testCollection.has('a')).toBe(true);
		});
	});

	describe('method "add"', () => {

		it('should add to the collection a new item', () => {
			testCollection.add('a');
			expect(testCollection.length).toBe(1);
		});

		it('should add to the collection an array of new items', () => {
			testCollection.add(['a', 'b', 'c']);
			expect(testCollection.length).toBe(3);
		});

		it('should add to the beginning of the collection an array of new items', () => {
			testCollection.add(['a', 'b', 'c']);
			testCollection.add('d', true);
			expect(testCollection.length).toBe(4);
			expect(testCollection.getCollectionAsArray()).toEqual(['d', 'a', 'b', 'c']);
		});
	});

	describe('method "remove"', () => {

		it('should throw an Error if wrong index is specified', () => {
			expect(() => testCollection.remove('s')).toThrow();
		});

		it('should remove the item from the collection', () => {
			const testValue = 'a';
			(testCollection as any).items = [testValue];
			expect(testCollection.length).toBe(1);
			testCollection.remove(testValue);
			expect(testCollection.length).toBe(0);
		});
	});
});
